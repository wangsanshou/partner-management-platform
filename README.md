## 简介
环球汇合作方平台

## 项目功能
- 使用Vue3.0开发，单文件组件采用＜script setup＞
- 采用 Vite3 作为项目开发、打包工具（配置了 Gzip 打包、TSX 语法、跨域代理）
- 整个项目集成了 TypeScript
- 登录逻辑，使用vue-router进行路由权限拦截，判断，路由懒加载
- 使用 keep-alive 对整个页面进行缓存，支持多级嵌套页面
- 侧边栏导航菜单栏动态的显示
- 各种可视化地图组件
- 使用 Pinia替代 Vuex，轻量、简单、易用
- 导出excel,自定义样式导出excel、多表头导出
- 使用 Prettier 统一格式化代码，集成 Eslint、代码校验规范

## 安装
```
  # Gitlab
  git clone 待补充
```

## 分支管理
  ```
  待补充
  ```

## 下载依赖
```
 npm install
 cnpm install
 yarn 
 # npm install 安装失败，请升级 nodejs 到 16 以上，或尝试使用以下命令：
  npm install --registry=https://registry.npm.taobao.org
```

## 运行打包
```
 npm run dev
 npm run build 
```

## eslint+prettier
```
# eslint 检测代码
npm run lint

#prettier 格式化代码
npm run lint:prettier
```

## 代码提交规范
```
统一代码提交规范：commit时需要按照"type: subject"(冒号后需有空格)的格式提交，否则报错;

type包含以下，根据需要可补充：
feat：新功能开发  
fix：bug修复问题  
docs：资源文档更新
style：代码风格样式调整（不影响代码逻辑）  
refactor：代码重构（既不是修复也不是新功能）  
perf：项目性能优化相关 
test：测试用例相关
chore：其他变更（构建过程或辅助工具等）  
revert：回滚版本
build：编译相关的修改，例如发布版本、对项目构建或者依赖的改动

eg: "feat: update menu"
```

## 文件目录结构
```
vue-admin-perfect
├─ public                 # 静态资源文件（忽略打包）
├─ src
│  ├─ api                 # API 接口管理
│  ├─ assets              # 静态资源文件
│  ├─ components          # 全局组件
│  ├─ config              # 全局配置项
│  ├─ hooks               # 常用 Hooks
│  ├─ language            # 语言国际化
│  ├─ layout              # 框架布局
│  ├─ routers             # 路由管理
│  ├─ store               # pinia store
│  ├─ styles              # 全局样式
│  ├─ utils               # 工具库
│  ├─ views               # 项目所有页面
│  ├─ App.vue             # 入口页面
│  └─ main.ts             # 入口文件
├─ .env                   # vite 常用配置
├─ .env.development       # 开发环境配置
├─ .env.production        # 生产环境配置
├─ .env.test              # 测试环境配置
├─ .eslintignore          # 忽略 Eslint 校验
├─ .eslintrc.cjs           # Eslint 校验配置
├─ .gitignore             # git 提交忽略
├─ .prettierignore        # 忽略 prettier 格式化
├─ .prettierrc.config.js         # prettier 配置
├─ index.html             # 入口 html
├─ yarn.lock      # 依赖包包版本锁
├─ package.json           # 依赖包管理
├─ README.md              # README 介绍
├─ tsconfig.json          # typescript 全局配置
└─ vite.config.ts         # vite 配置
```