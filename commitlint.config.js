module.exports = { 
    extends: ['@commitlint/config-conventional'],
    rules: {
        'header-max-length': [0, 'always', 72],
        'type-enum': [
            2,
            'always', 
            [ 
            'feat', // 新功能开发  
            'fix', // bug修复问题  
            'docs', // 资源文档更新
            'style', // 代码风格样式调整（不影响代码逻辑）  
            'refactor', // 代码重构（既不是修复也不是新功能）  
            'perf', // 项目性能优化相关 
            'test', // 测试用例相关
            'chore', // 其他变更（构建过程或辅助工具等）  
            'revert', // 回滚版本
            'build', // 编译相关的修改，例如发布版本、对项目构建或者依赖的改动
            ],
        ],
        'type-case': [0],
        'type-empty': [2, 'never'],
        'scope-case': [0], 
        'body-leading-blank': [2, 'always']
    },      
};
