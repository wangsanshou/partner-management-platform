import fs from 'fs'
import { langMap, langList } from '../../common/lang/langConfig'
const handleLang = () => {
    return {
      // 插件名称
      name: 'handle-lang',
      // pre 会较于 post 先执行
      enforce: 'pre', // post
      config(config: any, { mode, command }: any) {
        for(let key in langMap) {
          langMap[key] = langMap[key].split(":")
        }
        const langTypeList = langList.reduce((acc,item,index) => {
          let tempObj = {}
          for(let key in langMap) {
            tempObj[key] = langMap[key][index]
          }
          acc[item] = tempObj
          return acc
        },{})
        try {
          for(let lang in langTypeList) {
            let tempFieldStr = ""
            for(let field in langTypeList[lang]) {
              tempFieldStr += "  '" + field + "': '" + langTypeList[lang][field] + "',\n"
            }
            let tempContent = ""
            tempContent +="const "+ lang +" = {\n" +
              tempFieldStr +
            "}\n" +
            "export default " + lang
            fs.writeFileSync(`./src/common/lang/language/${lang}.ts`, tempContent);
          }
        } catch (err) {
          // console.error(err);
        }
      },
  
      configResolved(resolvedConfig: any) {},
  
      configureServer(server: any) {},
  
      transformIndexHtml(html: any) {},
    }
  }
export default handleLang