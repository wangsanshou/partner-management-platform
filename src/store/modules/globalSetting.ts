import { defineStore } from 'pinia'

export const useGlobalSettingStore = defineStore({
  id: 'themeState',
  state: () => ({
    themeName: 'light',
    gcpsLang: 'cn',
  }),
  getters: {},
  actions: {
    // 获取主题
    getTheme(): string{
        return this.themeName
    },
    // 设置主题
    setTheme(param: string) {
        this.themeName = param
    },
    getLangConfig(): string{
        return this.gcpsLang
    },
    setLangConfig(param: string) {
        this.gcpsLang = param
    },
  },
  persist: {
      // 本地存储的名称
      key: "gcps-global-setting",
      //保存的位置
      storage: window.localStorage,//localstorage
  },
})
