import { useSettingStore } from '@/store/modules/setting'

export const switchColor = () => {
  const SettingStore = useSettingStore()
  const themeConfig = computed(() => SettingStore.themeConfig)
  const body = document.documentElement as HTMLElement
  if (themeConfig.value.isDark) body.setAttribute('class', 'dark')
  else body.setAttribute('class', '')
}
