import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import pinia from './store'

// 权限路由
import './router/permission'
// svg-icons注册导入
import 'virtual:svg-icons-register'
import SvgIcon from '@/components/SvgIcon/index.vue' // svg component
// UI框架 element-plus
import ElementPlus from 'element-plus'
// 引入暗黑模式 element-plus 2.2 内置暗黑模式
import 'element-plus/theme-chalk/dark/css-vars.css'
// 自定义暗黑模式
import '@/common/themeStyle/dark.theme.scss'
import 'element-plus/dist/index.css'
// 引入阿里图标库
import '@/assets/iconfont/iconfont.css'
import '@/assets/iconfont/iconfont.js'
// 多语言配置
import I18n from '@/common/lang'
import './mock'
import createDirectives from './common/directives'

const app = createApp(App)

app.component('svg-icon', SvgIcon)

// 注册指令
createDirectives(app)

// 注册icon组件
import * as ElIconsModules from '@element-plus/icons-vue'
import { switchColor } from './utils/switchThemeColor'

// 全局注册element-plus icon图标组件
Object.keys(ElIconsModules).forEach((key) => {
  //循环遍历组件名称
  if ('Menu' !== key) {
    //如果不是图标组件不是Menu，就跳过，否则加上ICon的后缀
    app.component(key, ElIconsModules[key])
  } else {
    app.component(key + 'Icon', ElIconsModules[key])
  }
})
app.use(pinia)
app.use(router)
app.use(I18n)
app.use(ElementPlus).mount('#app')

// 进入页面加载持久化存储的主题色
switchColor()