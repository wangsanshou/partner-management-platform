/** When your routing table is too long, you can split it into small modules**/

import Layout from '@/layout/index.vue'

const userManagementRouter = [
    {
        path: '/userManagement',
        component: Layout,
        redirect: '/userManagement/userManagePage',
        name: 'userManagement',
        alwaysShow: true,
        meta: {
            title: '用户管理',
            icon: 'Avatar',
        },
        children: [
            {
                path: 'userManagePage',
                component: () => import('@/views/userManagement/userManagePage/index.vue'),
                name: 'userManagePage',
                meta: { title: '用户管理', keepAlive: true, icon: 'Operation' },
            },
            {
                path: 'personalCenter',
                component: () => import('@/views/userManagement/personalCenter/index.vue'),
                name: 'personalCenter',
                meta: { title: '个人中心', keepAlive: true, icon: 'User' },
            },
        ],
    },
]

export default userManagementRouter