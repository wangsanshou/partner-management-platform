/** When your routing table is too long, you can split it into small modules**/

import Layout from '@/layout/index.vue'

const partnerInfoRouter = [
    {
        path: '/partnerManagement',
        component: Layout,
        redirect: '/partnerManagement/partnerInfo',
        name: 'partnerManagement',
        alwaysShow: true,
        meta: {
            title: '合作方管理',
            icon: 'Grid',
        },
        children: [
            {
                path: 'partnerInfo',
                component: () => import('@/views/partnerManagement/partnerInfo/index.vue'),
                name: 'partnerInfo',
                meta: { title: '合作方信息', keepAlive: true, icon: 'ChatLineRound' },
            },
        ],
    },
]

export default partnerInfoRouter