/** When your routing table is too long, you can split it into small modules**/

import Layout from '@/layout/index.vue'

const reportQueryRouter = [
    {
        path: '/reportManagement',
        component: Layout,
        redirect: '/reportManagement/reportQuery',
        name: 'reportManagement',
        alwaysShow: true,
        meta: {
            title: '报表管理',
            icon: 'Histogram',
        },
        children: [
            {
                path: 'reportQuery',
                component: () => import('@/views/reportManagement/reportQuery/index.vue'),
                name: 'reportQuery',
                meta: { title: '报表查询', keepAlive: true, icon: 'Search' },
            },
        ],
    },
]

export default reportQueryRouter