/** When your routing table is too long, you can split it into small modules**/

import Layout from '@/layout/index.vue'

const custMangementRouter = [
  {
    path: '/custManagement',
    component: Layout,
    redirect: '/custManagement/custReview',
    name: 'custManagement',
    alwaysShow: true,
    meta: {
      title: '客户管理',
      icon: 'Management',
    },
    children: [
      {
        path: 'custReview',
        component: () => import('@/views/custManagement/accPendingReview/index.vue'),
        name: 'custReview',
        meta: { title: '开户待审核', keepAlive: true, icon: 'DataAnalysis' },
      },
      {
        path: 'custInfo',
        component: () => import('@/views/custManagement/custInfo/index.vue'),
        name: 'custInfo',
        meta: { title: '客户信息', keepAlive: true, icon: 'MessageBox' },
      },
      {
        path: 'merchantInfo',
        component: () => import('@/views/custManagement/merchantInfo/index.vue'),
        name: 'merchantInfo',
        meta: { title: '商户信息', keepAlive: true, icon: 'ScaleToOriginal' },
      },
    ],
  },
]

export default custMangementRouter
