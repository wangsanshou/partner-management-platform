/** When your routing table is too long, you can split it into small modules**/

import Layout from '@/layout/index.vue'

const applyFormQueryRouter = [
    {
        path: '/custTransation',
        component: Layout,
        redirect: '/custTransation/applyFormQuery',
        name: 'custTransation',
        alwaysShow: true,
        meta: {
            title: '客户交易',
            icon: 'TrendCharts',
        },
        children: [
            {
                path: 'applyFormQuery',
                component: () => import('@/views/custTransation/applyFormQuery/index.vue'),
                name: 'applyFormQuery',
                meta: { title: '申请单查询', keepAlive: true, icon: 'Search' },
            },
        ],
    },
]

export default applyFormQueryRouter