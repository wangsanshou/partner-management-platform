import http from './request'
import { paramData, tableData } from './paramsType'

export const testAPI = (params: paramData) =>
  http.request({ url: '/test', method: 'get', params: params })

export const testPAPI = (params: paramData) =>
  http.request({ url: '/testPost', method: 'post', params: params })

export const testPAPI2 = (params: paramData) =>
  http.request({
    url: '/testPost2',
    method: 'post',
    params: params,
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded',
    },
  })

export const testMock = (params: paramData) =>
  http.request({ url: '/mockApi/user/list', method: 'get' })

export const tableMock = (params: tableData) =>
  http.request({ url: '/mockApi/cusManagement/getTableDate', method: 'get' })
