import { App } from 'vue';
const modules: Record<string, () => Promise<unknown>> = import.meta.glob('./modules/*.ts')
export default (app: App): void => {
    for(let key in modules) {
        modules[key]().then((mod: unknown) => {
            app.directive(Object.keys(mod)[0], Object.values(mod)[0])
        })
    }
}