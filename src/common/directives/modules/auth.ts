import { useUserStore } from '@/store/modules/user'
import pinia from '@/store'
const roleList: Array<string> = ['admin','operator','customer']

// usage example: v-auth:admin | v-auth="['admin','operator']"
export const auth = async (el: HTMLElement | null, binding: any): Promise<void> => {
    let userRoles: Array<string> = await useUserStore(pinia).getRoles() as Array<string>
    const authListBool = binding.value && binding.value.some((authVal: string) => userRoles.includes(authVal))
    const authArgBool = userRoles.includes(binding.arg)
    if(!authListBool && !authArgBool) el.remove()
}