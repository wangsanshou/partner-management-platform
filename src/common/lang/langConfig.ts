const langList = ['cn', 'en', 'zh']
const langMap = {
    "common.submit": "提交:submit:提交",
    "common.success": "成功:Success:成功",
    "common.fail": "失败:fail:失敗",
    "common.text": "测试文本:Test text:測試文字",
}
export { langMap, langList }