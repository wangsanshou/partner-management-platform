import { createI18n } from 'vue-i18n'
import en from './language/en'
import cn from './language/cn'
import zh from './language/zh'

const messages = {
  en, // 英语
  cn, // 中文简体
  zh, // 中文繁体
}

let localLang = localStorage.getItem('gcps-global-setting')
localLang = localLang ? JSON.parse(localLang)['gcpsLang'] : "cn"
const i18n = createI18n({
    // 处理报错Uncaught (in promise) SyntaxError: Not available in legacy mode (at message-compiler.esm-bundler.js)
    legacy: false, 
    // 默认cn语言环境
    locale: localLang,
    //备用语言环境
    fallbackLocale: "en",  
    // 多语言配置
    messages
})

export default i18n