const data = []
// 表格静态mock数据
for (let i = 0; i < 100; i++) {
  data.push({
    date: '2016-05-02',
    cusName: '客户' + i,
    applyUser: 1 + i,
    failCount: '失败' + i,
    realState: '状态' + i,
    state: i % 2 ? 1 : 0,
    checked: true,
    id: i + 1,
    organize: '机构' + i,
    riskResult: '结果' + i,
    sucessCount: i + '次',
    reason: '臭弟弟',
    type: '入网类型',
    riskRank: '风险等级',
    channel: '伞形账户渠道',
    currency: '伞形账户币种',
  })
}
export const tableList = [...data]
