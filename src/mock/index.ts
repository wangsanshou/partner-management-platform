import Mock from 'mockjs';  
import { tableList } from './dataSource/table';
const baseMockUrl: string = import.meta.env.VITE_BASE_REQUEST_URL + '/mockApi'


Mock.mock( baseMockUrl + '/user/list', 'get', { 
  'statusCode': 200,  
  'message': 'success',  
  'data|1-10': [{  
    'id|+1': 1,  
    'name': '@cname',  
    'age|18-60': 1,  
    'email': '@email'  
  }]  
});  
Mock.mock( baseMockUrl + '/cusManagement/getTableDate', 'get', { 
  'statusCode': 200,  
  'message': 'success',  
  'data': tableList
  
});  

if(import.meta.env.MODE == 'development') {
    Mock.setup({ timeout: '600-1000' });
}