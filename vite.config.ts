import { defineConfig, ConfigEnv, UserConfig, loadEnv } from 'vite'
import path from 'path'
// vite.config.ts中无法使用import.meta.env 所以需要引入
import vue from '@vitejs/plugin-vue'
import { createSvgIconsPlugin } from 'vite-plugin-svg-icons'
// 增加 vue文件 script name值
import vueSetupExtend from 'vite-plugin-vue-setup-extend'
// 生产gz文件
import viteCompression from 'vite-plugin-compression'
// 自动导入vue中hook reactive ref等
import AutoImport from 'unplugin-auto-import/vite'
//自动导入ui-组件 比如说ant-design-vue  element-plus等
import Components from 'unplugin-vue-components/vite'
// element
import { ElementPlusResolver } from 'unplugin-vue-components/resolvers'
// 自定义vite插件
import handleLang from './src/plugins/vite/vite-plugin-handleLang'

function resolve(dir: string) {
  return path.join(__dirname, '.', dir)
}

// https://vitejs.dev/config/
export default defineConfig(({ mode }: ConfigEnv): UserConfig => {
  return {
    envDir: 'src/env/',
    plugins: [
      handleLang(),
      vue(),
      vueSetupExtend(),
      AutoImport({
        // 按需自动引入API方法
        imports: ['vue', 'vue-router', 'pinia'],
        // 全局声明的生成路径
        dts: 'src/@declareTypes/auto-import-api.d.ts',
        // element全局方法解析
        resolvers: [ElementPlusResolver()],
      }),
      Components({
        // 按需引入组件
        // 全局声明的生成路径components.d.ts
        // dts: "src/@declareTypes/components.d.ts",
        dts: false,
        // element组件类型解析
        resolvers: [ElementPlusResolver()],
      }),
      // * 使用 svg 图标
      createSvgIconsPlugin({
        // 指定需要缓存的图标文件夹
        iconDirs: [path.resolve(process.cwd(), 'src/common/icons/svg')],
        // 指定symbolId格式
        symbolId: 'icon-[dir]-[name]',
      }),
      // gzip压缩 生产环境生成 .gz 文件
      mode === 'production' &&
        viteCompression({
          verbose: true,
          disable: false,
          threshold: 10240,
          algorithm: 'gzip',
          ext: '.gz',
        }),
    ],
    css: {
      preprocessorOptions: {
        scss: {
          // 所有模块导入全局主题样式，别名为 “ * ” ，可直接调用，不需要添加命名空间（相比于@import，可实现样式去重）
          additionalData: `@use "./src/assets/styles/index.scss" as *;`,
        },
      },
    },
    // 配置别名
    resolve: {
      alias: {
        '@': resolve('src'),
        static: resolve('public/static'),
      },
      // 导入模块时忽略后缀名, 添加 .vue 选项时要记得原本默认忽略的选项也要手动写入
      extensions: ['.mjs', '.js', '.ts', '.jsx', '.tsx', '.json', '.vue'],
    },
    //启动开发服务配置
    server: {
      // 服务器主机名，如果允许外部访问，可设置为 "0.0.0.0" 也可设置成你的ip地址
      host: '0.0.0.0',
      port: 8080,
      open: true,
      https: false,
      cors: true,
      // 代理跨域
      proxy: {
        '/api': {
          target: 'http://localhost:3040',
          changeOrigin: true,
          rewrite: (path) => path.replace(/^\/api/, ''),
        },
      },
    },
    esbuild: {
      //生产环境去除 console debugger
      drop: mode === 'production' ? ['console', 'debugger'] : [],
    },
    preview: {
      open: './gcps-partner/index.html',
    },
    build: {
      outDir: 'gcps-partner',
      // 使用teser压缩打包
      minify: 'terser',
      terserOptions: {
        compress: {
          drop_console: true,
          drop_debugger: true,
        },
      },
      rollupOptions: {  
        // input: 'index.html',
        external: ['mockjs'], 
        output: {  
          globals: {  
            'mockjs': 'empty' // 将 mock 模块映射到一个空对象
          }  
        }  
      } 
    },
  }
})
