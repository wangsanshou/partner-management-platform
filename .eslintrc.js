module.exports = {
  env: {
    browser: true,
    es2021: true,
    node: true,
  },
  extends: [
    'eslint:recommended',
    'plugin:vue/vue3-essential',
    'plugin:@typescript-eslint/recommended',
    'plugin:prettier/recommended',
  ],
  parser: 'vue-eslint-parser',
  parserOptions: {
    ecmaVersion: 'latest',
    parser: '@typescript-eslint/parser',
    sourceType: 'module',
  },
  ignorePatterns: ['**/assets/iconfont/*.js'],
  plugins: ['vue', '@typescript-eslint'],
  rules: {
    // 原生eslint、插件校验规则
    'no-var': 'error',
    '@typescript-eslint/no-this-alias': 'off',
    'vue/multi-word-component-names': 'warn',
    '@typescript-eslint/no-empty-function': 'warn',
    'vue/no-deprecated-v-on-native-modifier': 'warn',
    '@typescript-eslint/no-explicit-any': 'off',
    'no-useless-escape': 'warn',
    '@typescript-eslint/ban-ts-comment': 'off',
    'no-async-promise-executor': 'warn',
  },
}
